let num = 2
const getCube = num ** 3

console.log(`The cube of ${num} is ${getCube}`)

const address = {
	houseNumber: 258,
	street: 'Washington Ave NW,',
	state: 'California',
	zip: 90011,
};

const {houseNumber, street, state, zip} = address;

console.log(`I live at ${houseNumber} ${street} ${state} ${zip}`);

const animal = {
	name: 'Lolong'
	type: 'saltwater crocodile'
	weight: '1075 kg'
	height: '20 ft 3 in.'
};

const {name, type, weight, height} = animal;

console.log(`${name} was a ${type}. He weighed at ${weight}`);

